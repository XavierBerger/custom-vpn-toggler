#!/bin/bash
#
# Copyright (c) 2021 Xavier Berger
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

#
# Support script for FortiClient from Fortinet 
# Note: Use FortiClient for initial configuration.
#
# Requirements:
# * FortiClient suite from fortinet
# * expect command

if [ -z $1 ]
then
	exit 1
fi

case $1 in

"start")
	password=$(zenity --password)
	if [ ! -z ${password} ] 
	then
		password=${password} /usr/bin/expect << 'EOS'
spawn /opt/forticlient/fortivpn connect MyVpnName --user=MyUserName
expect "password:"
send $::env(password)
send "\r"
expect eos;
EOS
	fi
;;

"stop" )
	/opt/forticlient/fortivpn disconnect
;;

"ip")
	/opt/forticlient/fortivpn status | grep IP | cut -d" " -f 4
;;
esac
