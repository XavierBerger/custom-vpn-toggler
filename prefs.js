/*
 * Copyright (c) 2021 Xavier Berger
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
const Gtk = imports.gi.Gtk;
const Gio = imports.gi.Gio;

const ExtensionUtils = imports.misc.extensionUtils;
const Extension = ExtensionUtils.getCurrentExtension();

function init() {
    ExtensionUtils.initTranslations();
}

function buildPrefsWidget() {

    let settings = ExtensionUtils.getSettings();

    let prefs = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL,
        margin_top: 8,
        margin_bottom: 8
    });
    prefs.set_spacing(16);

    {
        const frameVpn = _addFrame(prefs);
        const keyName = "vpn"
        let key = settings.settings_schema.get_key(keyName);
        let widget = new Gtk.Entry({ width_chars: 16, tooltip_text: key.get_description() });
        widget.set_text(settings.get_string(keyName));
        widget.connect('changed', function () {
            settings.set_string(keyName, widget.get_text());
        });
        _addSetting(frameVpn, key, widget);

        let help = new Gtk.Label({
            label: "\n<small>" +
                "<b>VPN command have to implement the following parameters</b>:\n" +
                " - <b>start</b>: to start VPN - If required, this command could display GUI.\n" +
                " - <b>stop</b>: to stop VPN.\n" +
                " - <b>ip</b>: to get IP address - If not IP is available, this function should return nothing." +
                "\n" +
                "<i>Note: parameter <b>ip</b> is used to determine if VPN is started or not.</i>" +
                "</small>\n",
            use_markup: true
        });
        frameVpn.append(help);

        let link = new Gtk.LinkButton({
            label: "See example of scripts in GitLab",
            visible: true,
            receives_default: true,
            halign: Gtk.Align.CENTER,
            uri: "https://gitlab.com/XavierBerger/custom-vpn-toggler/-/tree/master/scripts"
        });
        frameVpn.append(link);
    }

    {
        const frameCheckTime = _addFrame(prefs);
        const keyName = "checktime"
        let key = settings.settings_schema.get_key(keyName);
        let widget = new Gtk.SpinButton({ tooltip_text: key.get_description() });
        widget.set_range(1, 900);
        widget.set_increments(1, 5);
        widget.set_value(settings.get_uint(keyName));
        widget.connect('value-changed', function () {
            settings.set_uint(keyName, widget.get_value());
        });
        _addSetting(frameCheckTime, key, widget);
    }

    {
        const frameIpNotify = _addFrame(prefs);
        const keyName = "ipnotify"
        let key = settings.settings_schema.get_key(keyName);
        let widget = new Gtk.Switch({
            tooltip_text: key.get_description(),
            halign: Gtk.Align.END,
            valign: Gtk.Align.CENTER,
        });
        settings.bind(keyName, widget, "active", Gio.SettingsBindFlags.DEFAULT);
        _addSetting(frameIpNotify, key, widget);
    }

    {
        let about = new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
            margin_top: 8,
            margin_bottom: 8
        });
        about.set_spacing(0);

        let title = new Gtk.Label({
            label: "<b>Custom Vpn Toggler</b>",
            halign: Gtk.Align.CENTER,
            use_markup: true
        });
        about.append(title);

        let version = new Gtk.Label({
            label: "Version: " + Extension.metadata.version.toString()
        });
        about.append(version);

        let link = new Gtk.LinkButton({
            label: "GitLab",
            visible: true,
            receives_default: true,
            halign: Gtk.Align.CENTER,
            uri: "https://gitlab.com/XavierBerger/custom-vpn-toggler"
        });
        about.append(link);

        let warranty = new Gtk.Label({
            label: "<sup>This program is provided \"as is\" with no warranty of any kind. </sup>",
            halign: Gtk.Align.CENTER,
            use_markup: true
        });
        about.append(warranty);

        let license = new Gtk.Label({
            label: "<sup>This program is published under a MIT license.</sup>",
            halign: Gtk.Align.CENTER,
            use_markup: true
        });
        about.append(license);

        prefs.append(about);
    }

    return prefs;
}

function _addFrame(prefs) {
    let frame = new Gtk.Frame({
        can_focus: true,
        hexpand: true,
    });

    list = new Gtk.ListBox({
        can_focus: true,
        hexpand: true,
        selection_mode: Gtk.SelectionMode.NONE,
    });
    frame.set_child(list);
    prefs.append(frame);
    return list;
}

function _addSetting(list, key, widget) {
    let grid = new Gtk.Grid({
        can_focus: true,
        column_spacing: 12,
        marginStart: 24,
        marginTop: 8,
        marginBottom: 8,
        marginEnd: 8,
        vexpand: false,
        valign: Gtk.Align.START
    });
    let label = new Gtk.Label({
        label: key.get_summary(),
        xalign: 0,
        halign: Gtk.Align.FILL,
        hexpand: true
    });
    let description = new Gtk.Label({
        label: key.get_description(),
        xalign: 0,
        halign: Gtk.Align.FILL,
        hexpand: true
    });
    description.get_style_context().add_class("dim-label");
    grid.attach(label, 0, 0, 1, 1);
    grid.attach(description, 0, 1, 1, 1);
    grid.attach(widget, 1, 0, 1, 2);

    list.append(grid);
}
